package najdrowski.pl

import io.ktor.server.application.*
import io.ktor.server.plugins.*
import najdrowski.pl.plugins.*


fun main(args: Array<String>): Unit =
    io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused")
fun Application.module() {
    install(CallLogging)
    configureRouting()
}
